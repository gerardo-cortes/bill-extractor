import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="bill-extractor",
    version="0.1.0",
    author="Gerardo Cortes",
    author_email="gcortes@interware.com.mx",
    description="Bill information extractor",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/gerardo-cortes",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
