#!/usr/bin/env bash

#if [ $# -ne 2 ]; then
#  echo "Usage bill-extractor.sh <varA> <varB>"
#    exit 1
#fi

export APP_ARGS=("$@")
export APP_ARGS=("${APP_ARGS[@]/#/\"}")
export APP_ARGS=${APP_ARGS[@]/%/\",}

echo $APP_ARGS

export STARTER="mx.interware.main.starter"

export BIN_PATH="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "BIN path $BIN_PATH"
export ROOT_PATH=$(dirname $BIN_PATH)
echo "Starting application from $ROOT_PATH"
cd $ROOT_PATH

python -c "from $STARTER import main; main($APP_ARGS)"
