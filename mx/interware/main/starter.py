# starter.py
# mx.interware.main.starter module
import sys
from builtins import print

import mx.interware.image.agent as agent
import mx.interware.image.contours as contour
import mx.interware.image.matcher as matcher

from mx.interware.util.logutil import getlogger

log = getlogger("mx.interware.main.starter")

print(sys.path)

root_path = "/home/axis/Development/Interware/cti/pocs/automation/projects/bill-extractor/"

def do_some_extraction():
    # image_name = "data/pdf/966020500214_ene.pdf"
    image_path = root_path + "data/png/shapes.png"
    extracted_info = agent.extract(image_path)
    log.info("extracted_info: {}".format(extracted_info))

def do_some_processing():
    pdf_file = root_path + "data/pdf/966020500214_ene.pdf"
    png_file = pdf_file + "_0.png"

    images = contour.extract_images(pdf_file)
    log.info("images : {}".format(images))
    log.info("img-0 : {}".format(images[0]))
    images[0].save(png_file, "PNG")
    contour.process_image(png_file)

def do_some_match():
    # image_name = "data/pdf/966020500214_ene.pdf"
    image_path = root_path + "data/png/cfe.png"
    template_path = root_path + "data/png/rnd_tem.png"
    matcher.do_match(image_path, template_path)


def main(arg1=None, arg2=None):
    # print("arg1 : ", arg1)
    # print("arg2 : ", arg2)

    log.info("Starting the thing ...")
    # time.sleep(3)
    log.info("After sleep.")
    #do_some_extraction()
    #do_some_processing()
    do_some_match()

if __name__ == '__main__':
    main(sys.argv)
