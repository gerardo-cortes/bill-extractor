# vehicles.py
# mx.interware.test.vehicles module


import sys
import time

from mx.interware.util.logutil import getlogger

log = getlogger("mx.interware.test.vehicles")

print(sys.path)


class Car:
    def __init__(self):
        self.speed = 0
        self.odometer = 0
        self.time = 0

    def say_state(self):
        log.info("I'm going {} kph!".format(self.speed))
        log.info("Current speed    : {}".format(self.speed))
        log.info("Current odometer : {}".format(self.odometer))
        log.info("Current time     : {}".format(self.time))

    def accelerate(self):
        self.speed += 5
        self.step()

    def brake(self):
        self.speed -= 5
        self.step()

    def step(self):
        self.odometer += self.speed
        self.time += 1

    def average_speed(self):
        if self.time == 0:
            return 0
        else:
            return self.odometer / self.time


def doit():
    log.info('doing the thing ...')
    number = 0

    for number in range(10):
        number = number + 1

        if number == 5:
            log.info("OK GO!!")
            break  # pass here

        time.sleep(0.5)
        log.info('Number is ' + str(number))

    log.info('Out of loop')


if __name__ == '__main__':
    doit()
