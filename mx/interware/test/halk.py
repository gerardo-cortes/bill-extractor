# halk.py
# Halk test module
import mx.interware.util.parse as parse
from mx.interware.test.vehicles import *
from mx.interware.util.fileutil import build_path, write_json
from mx.interware.util.logutil import getlogger
from mx.interware.util.parse import LineCounter

log = getlogger("mx.interware.test.halk")


def main1():
    car = Car()
    print("I'm a car!")
    while True:
        action = input("What should I do? [A]ccelerate, [B]rake, "
                       "show [O]dometer, show average [S]peed, e(x)it? ").upper()
        if action not in "ABOSX" or len(action) != 1:
            print("I don't know how to do that")
            continue
        print("action   : " + action)
        if action == 'A':
            car.accelerate()
        elif action == 'B':
            car.brake()
        elif action == 'O':
            print("The car has driven {} kilometers".format(car.odometer))
        elif action == 'S':
            print("The car's average speed was {} kph".format(car.average_speed()))
            car.step()
        elif action == 'X':
            print("Halting ...")
            break
        car.say_state()


def main2():
    file_path = build_path("data/txt/test-01.txt")
    line_counter = LineCounter(file_path)
    line_counter.count()
    log.info("Before reading file, lines : {}".format(line_counter.lines))
    log.info("Line count                 : {}".format(line_counter.count()))
    line_counter.read()
    log.info("After reading file, lines  : {}".format(line_counter.lines))
    log.info("Line count                 : {}".format(line_counter.count()))

    example_lines = parse.read(file_path)
    lines_count = parse.count(example_lines)
    log.info("Line count (functional)    : {}".format(line_counter.count()))


def main3():
    dictionary = {'dictA': {'key_1': 'value_1'},
                  'dictB': {'key_2': 'value_2'}}
    print("Dictionary : {}".format(dictionary))
    log.info("Dictionary : {}".format(dictionary))

    json_path = build_path("data/json/dictionary.json")
    write_json(json_path, dictionary)


if __name__ == '__main__':
    # main1()
    main2()
    # main3()
