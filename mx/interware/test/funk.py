from functools import reduce as reduce

from mx.interware.util.logutil import getlogger

log = getlogger("mx.interware.test.funk")


# This is the outer enclosing function
def print_msg_1(msg):
    # This is the nested function
    def printer():
        log.info(msg)

    printer()


# This is the outer enclosing function
def print_msg_2(msg):
    # This is the nested function
    def printer():
        log.info(msg)

    # this got changed
    return printer


def add_it(x, y):
    return (x + y)


def get_lenght(n):
    return len(n)


def min6(value):
    return value >= 6


def xprint(value):
    log.info("value : {}".format(value))


def main1():
    print_msg_1("Hello there!")


def main2():
    another = print_msg_2("Hello again!!")
    another()


def main3():
    my_list = [1, 2, 3, 4, 5]
    sum = reduce(add_it, my_list)
    log.info("sum : {}".format(sum))

    fruit_list = ('apple', 'banana', 'pineapple', 'cherry', 'pear')
    log.info("fruit_list : {}".format(fruit_list))
    lenghts = list(map(get_lenght, fruit_list))
    log.info("lenghts : {}".format(lenghts))
    list(map(xprint, fruit_list))

    filtered = list(filter(min6, lenghts))
    log.info("filtered : {}".format(filtered))


if __name__ == '__main__':
    # main1()
    # main2()
    main3()
