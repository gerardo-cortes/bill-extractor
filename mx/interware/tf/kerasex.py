import tensorflow as tf

mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

model = [tf.keras.layers.Flatten(),
         tf.keras.layers.Dense(512, activation=tf.nn.relu),
         tf.keras.layers.Dropout(0.2),
         tf.keras.layers.Dense(10, activation=tf.nn.softmax)]

# training arguments
optimizer = 'adam'
loss = 'sparse_categorical_crossentropy'
metrics = ['accuracy']

xmodel = tf.keras.models.Sequential(model)
xmodel.compile(optimizer, loss, metrics)
xmodel.fit(x_train, y_train, epochs=5)
result = xmodel.evaluate(x_test, y_test)

print("result : {}".format(result))
