# contours.py
# mx.interware.image.contours module

import os.path
import time
import cv2
from pdf2image import convert_from_path

from mx.interware.util.logutil import getlogger

log = getlogger("mx.interware.image.contours")


def extract_images(pdf_file):
    path = os.path.abspath(pdf_file)
    dir_path = os.path.dirname(path)
    log.info("dir_path : {}".format(dir_path))
    #images = convert_from_path(pdf_file, fmt='png', output_folder=dir_path)
    images = convert_from_path(pdf_file, fmt='png')
    return images


def process_image(image_path):
    log.info("Processing image : {} ...".format(image_path))
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    ret, thresh_image = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

    cv2.imshow("thresh", thresh_image)
    cv2.imwrite("/home/axis/Desktop/tmp-0.png", thresh_image)

    # contours,h = cv2.findContours(thresh,1,2)
    contours, hierarchy = cv2.findContours(thresh_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    log.info("contours len : {}".format(len(contours)))
    time.sleep(30)

    for contour in contours:
        log.info("contour : {}".format(contour))
        approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
        print(len(approx))
        if (len(approx) == 5) and (len(contour) > 10):
            print("pentagon")
            cv2.drawContours(image, [contour], 0, 255, -1)
        # if len(approx)==3:
        #    print("triangle")
        #    cv2.drawContours(img,[cnt],0,(0,255,0),-1)
        elif (len(approx) == 4) and (len(contour) > 10):
            print("square:" + str(contour) + ":" + str(len(contour)))
            cv2.drawContours(image, [contour], 0, (0, 0, 255), -1)
        # elif len(approx) == 9:
        #    print("half-circle")
        #    cv2.drawContours(img,[cnt],0,(255,255,0),-1)
        # elif len(approx) > 15:
        #    print("circle")
        #    cv2.drawContours(img,[cnt],0,(0,255,255),-1)

    cv2.imshow('img', image)
    cv2.imwrite("/home/axis/Desktop/tmp-1.png", image)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
