# starter.py
# mx.interware.image.agent module

import cv2

from mx.interware.util.logutil import getlogger

log = getlogger("mx.interware.image.agent")
font = cv2.FONT_HERSHEY_COMPLEX


def extract(image_path):
    log.info("Reading image : {} ...".format(image_path))
    image = cv2.imread(image_path)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    retval, bw_image = cv2.threshold(gray_image, 245, 255, cv2.THRESH_BINARY)
    # retval, threshold = cv2.threshold(image, 100, 255, cv2.THRESH_TOZERO)
    log.info("retval : {}".format(retval))

    # _, contours = cv2.findContours(bw_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # _, contours = cv2.findContours(bw_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    _, contours = cv2.findContours(bw_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    log.info("contours len : {}".format(len(contours)))

    for contour in contours:
        log.info("contour : {}".format(contour))
        #approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
        #print(len(approx))
        #if (len(approx) == 5) and (len(contour) > 10):
            #print("pentagon")
            #cv2.drawContours(gray_image, [contour], 0, 255, -1)


        # arc_length = cv2.arcLength(contour, True)
        # log.info("arc_length : {}".format(arc_length))
        # approx = cv2.approxPolyDP(contour, 0.01 * arc_length, True)
        # cv2.drawContours(image, [approx], 0, (0), 5)
        # x = approx.ravel()[0]
        # y = approx.ravel()[1]
        # if len(approx) == 3:
        #     cv2.putText(image, "Triangle", (x, y), font, 1, (0))
        # elif len(approx) == 4:
        #     cv2.putText(image, "Rectangle", (x, y), font, 1, (0))
        # elif len(approx) == 5:
        #     cv2.putText(image, "Pentagon", (x, y), font, 1, (0))
        # elif 6 < len(approx) < 15:
        #     cv2.putText(image, "Ellipse", (x, y), font, 1, (0))
        # else:
        #     cv2.putText(image, "Circle", (x, y), font, 1, (0))

    cv2.imshow("image", image)
    cv2.imshow("gray_image", gray_image)
    cv2.imshow("threhold", bw_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return {'A': 4139, 'B': 4127, 'C': 4098}
