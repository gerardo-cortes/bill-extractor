import dis


def f1(line):
    a, b, c = line.split()


def f2(line):
    (a, b, c) = line.split()


def f3(line):
    [a, b, c] = line.split()


def main1():
    f1str = dis.dis(f1)
    f2str = dis.dis(f2)
    f3str = dis.dis(f3)
    print("f1str == f2str : {}".format(f1str == f2str))
    print("f2str == f3str : {}".format(f2str == f3str))


if __name__ == '__main__':
    main1()
