import json
import os

from definitions import ROOT_DIR


def build_path(file_name):
    return ''.join([ROOT_DIR, "/", file_name])


def write_json(json_path, dictionary):
    json_str = json.dumps(dictionary)

    json_dir = os.path.dirname(json_path)
    if not os.path.exists(json_dir):
        os.makedirs(json_dir)

    open(json_path, 'a').close()
    f = open(json_path, "w")
    f.write(json_str)
    f.close()
