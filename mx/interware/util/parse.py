from mx.interware.util.logutil import getlogger

log = getlogger("mx.interware.util.parse")


class LineCounter:

    def __init__(self, file_path):
        log.info("Initializing. file_path : {} ...".format(file_path))
        self.file = open(file_path, 'r')
        self.lines = []

    def read(self):
        log.info("Reading file ...")
        self.lines = [line for line in self.file]

    def count(self):
        log.info("Counting lines ...")
        return len(self.lines)


def read(filename):
    with open(filename, 'r') as f:
        return [line for line in f]


def count(lines):
    return len(lines)
