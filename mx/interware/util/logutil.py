# logutil.py
# mx.interware.util.logutil module

import logging
from enum import Enum

from mx.interware.util.fileutil import build_path


class LogOutput(Enum):
    standard_out = 1
    file = 2
    both = 3


def getlogger(name, mode=None):
    logger = logging.getLogger(name)
    logging.addLevelName(logging.DEBUG, "DEBUG")
    logging.addLevelName(logging.INFO, "INFO ")
    logging.addLevelName(logging.WARNING, "WARN ")
    logging.addLevelName(logging.ERROR, "ERROR")
    logging.addLevelName(logging.CRITICAL, "FATAL")

    logger.setLevel(logging.DEBUG)
    log_format = ':csh %(asctime)s %(levelname).5s [%(process)d] [%(threadName)s] [%(module)s] %(name)s:%(lineno)d - %(message)s'

    if mode == LogOutput.file or mode == LogOutput.both or mode is None:
        file_handler = logging.FileHandler(build_path('logs/bill-extractor.log'))
        formatter = logging.Formatter(log_format)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    if mode == LogOutput.standard_out or mode == LogOutput.both or mode is None:
        sout_handler = logging.StreamHandler()
        formatter = logging.Formatter(log_format)
        sout_handler.setFormatter(formatter)
        logger.addHandler(sout_handler)

    return logger

