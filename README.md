# bill-extrator

Bill Extractor v0.1.0

Tools for extracting billing information

## For installing Python virtual environments

```
sudo apt install python3-venv
```

## For creating Python virtual environment

On project root path:

```
python3 -m venv ./venv
```



## To activate/deactivate python virtual environment:
```
source venv/bin/activate

...
pip install -r requirements.txt
...

deactivate
```

## For running the project:
```
bin/bill-extractor.sh
```

## For packaging the project:
```
python setup.py sdist bdist_wheel
```
